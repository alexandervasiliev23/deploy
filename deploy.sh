#!/usr/bin/env bash

cd /var/www/deploy

git fetch --all
git reset --hard origin/master
git pull origin master

docker-compose up -d --build
